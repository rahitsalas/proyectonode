var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color;
};


bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb) {
    console.log(aBici);
    return this.add(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({ code: aCode }, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({ code: aCode }, cb);
};

// bicicletaSchema.statics.updateByCode = function(aCode, cb) {
//     return this.updateOne({ code: aCode }, cb);
// }

module.exports = mongoose.model('Bicicleta', bicicletaSchema);






// var Bicicleta = function(id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function() {
//     return 'id: ' + this.id + "| color:" + this.color;
// }

// Bicicleta.allBicis = [];

// Bicicleta.add = function(Bici) {
//     Bicicleta.allBicis.push(Bici);
// }

// Bicicleta.findById = function(BiciId) {
//     var Bici = Bicicleta.allBicis.find(x => x.id == BiciId)
//     if (Bici)
//         return Bici;
//     else
//         throw new Error(`No existe una bicicleta con el id ${BiciId}`);
// }

// Bicicleta.removeById = function(BiciId) {
//     for (var i = 0; i < Bicicleta.allBicis.length; i++) {
//         if (Bicicleta.allBicis[i].id == BiciId) {
//             Bicicleta.allBicis.splice(i, 1);
//             break;
//         }
//     }
// }



// // var a = new Bicicleta(1, 'rojo', 'urbana', [-16.4051838, -71.5833646]);
// // var b = new Bicicleta(2, 'verde', 'urbana', [-16.404523, -71.584548]);

// // Bicicleta.add(a);
// // Bicicleta.add(b);

// module.exports = Bicicleta;
var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.find({}, function(err, bicicletas) {
        res.status(200).json({
            bicicletas: bicicletas
        });
    });
}

exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo });
    bici.ubicacion = [req.body.lat, req.body.lng];
    bici.save(function(err) {
        res.status(200).json(bici);
    });
    // var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    // bici.ubicacion = [req.body.lat, req.body.lng];
    // Bicicleta.add(bici);
    // res.status(200).json({
    //     bicicleta: bici
    // });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.findByCode(req.body.code, function(err, bicicleta) {
        console.log(bicicleta);
        Bicicleta.removeByCode(req.body.code, function(err, bicicleta) {
            console.log(bicicleta);
            res.status(204).send();
        });
    });
}

// exports.bicicleta_update = function(req, res) {
//     Bicicleta.findByCode(req.body.code, function(err, bicicleta) {
//         console.log(bicicleta);
//         Bicicleta.updateByCode(req.body.code, function(err, bicicleta) {
//             console.log(bicicleta);
//             res.status(204).send();
//         });
//     });
// }
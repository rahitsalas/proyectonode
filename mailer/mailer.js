const nodemailer = require ('nodemailer');
const sgTransport = require ('nodemailer-sendgrid-transport');

let mailConfig;

if(process.env.NODE_ENV ===  'production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    if(process.env.NODE_ENV === 'staging'){
        console.log('XXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    } else {
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: process.env.ethereal_user, // generated ethereal user velda.cormier@ethereal.email
                pass: process.env.ethereal_pwd// generated ethereal password kbUrpQF8NmmZRqKsZT
            }
        }
    }
}


// const mailConfig = {
//     host: 'smtp.ethereal.email',
//     port: 587,
//     secure: false, // true for 465, false for other ports
//     auth: {
//         user: 'velda.cormier@ethereal.email', // generated ethereal user velda.cormier@ethereal.email
//         pass: 'kbUrpQF8NmmZRqKsZT' // generated ethereal password kbUrpQF8NmmZRqKsZT
//     }
// };

module.exports = nodemailer.createTransport(mailConfig);
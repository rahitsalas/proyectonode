var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function() {
    beforeEach(function(done) {

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true
        });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('Estamos conectados a la base de prueba');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);

        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('borra una instancia de bicicleta', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                Bicicleta.removeByCode(1);
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe retornar una bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({ code: 2, color: "roja", modelo: "urbana" });
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, target) {
                            expect(target.code).toBe(aBici.code);
                            expect(target.color).toBe(aBici.color);
                            expect(target.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

});
// beforeEach(() => {
//     Bicicleta.allBicis = [];
// })

// describe('Bicicleta.allBicis', () => {
//     it('comienza  vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);


//         var a = new Bicicleta(1, 'rojo', 'urbana', [-16.4051838, -71.5833646]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// })

// describe('Bicicleta.findById', () => {
//     it('debe devolver la bici con id = 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1, 'rojo', 'urbana', [-16.4051838, -71.5833646]);
//         var b = new Bicicleta(2, 'verde', 'urbana', [-16.404523, -71.584548]);

//         Bicicleta.add(b);
//         Bicicleta.add(a);

//         var target = Bicicleta.findById(1);
//         expect(target.id).toBe(1);
//         expect(target.color).toBe(a.color);
//         expect(target.modelo).toBe(a.modelo);
//     });
// })
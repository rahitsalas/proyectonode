var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = "http://localhost:5000/api/bicicletas";

describe('Bicicleta API', () => {

    beforeEach(function(done) {

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true
        });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('Estamos conectados a la base de prueba');
            done();
        });
    });
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    // describe('GET BICICLETAS', () => {
    //     it('Status 200', () => {
    //         expect(Bicicleta.allBicis.length).toBe(0);
    //         var a = new Bicicleta(1, 'negro', 'urbano', [-16.4051838, -71.5833646]);
    //         Bicicleta.add(a);

    //         request.get('http://localhost:5000/api/bicicletas'),
    //             function(error, response, body) {
    //                 expect(response.statusCode).toBe(200);
    //             };

    //     });
    // });
    describe("GET BICICLETAS /", () => {
        it("Status 200", (done) => {
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                console.log(result);
                expect(response.statusCode).toBe(200);
                //expect(result.length).toBe(0);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });


    describe("POST BICICLETAS /CREATE", () => {
        it("Status 200", (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code" : 1, "color": "rojo", "modelo": "urbana", "lat":-34, "lng":-54 }';
            console.log(aBici);
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body);
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe("DELETE BICICLETAS /DELETE", () => {
        it("Status 200", (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{ "code" : 1, "color": "rojo", "modelo": "urbana", "lat":-34, "lng":-54 }';
            console.log(aBici);
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body);
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });

            var code = '{ "code" : 1 }';

            request.delete({
                headers: headers,
                url: base_url + '/delete',
                body: code
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toBe(0);
                });
            });
        });

    });
});

// describe('POST BICICLETAS /CREATE', () => {
//     describe('GET BICICLETAS', () => {
//         it('Status 200', (done) => {
//             var headers = { 'content-type': 'application/json' };
//             var a = '{"id":10, "color": "rojo", "modelo":"urbana", "lat": -34, "lng": -54}';
//             request.post({
//                 headers: headers,
//                 url: 'http://localhost:5000/api/bicicletas/create',
//                 body: a
//             }, function(error, response, body) {
//                 expect(response.statusCode).toBe(200);
//                 expect(Bicicleta.findById(10).color).toBe("rojo");
//                 done();
//             });

//         });
//     });
// });